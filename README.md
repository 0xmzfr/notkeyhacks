# NotKeyHacks

NotKeyHacks is the opposite of the [KeyHacks](https://github.com/streaak/keyhacks) repository by [@streaak](https://twitter.com/streaak/). Sensitive tokens are fun, but a lot of time is wasted reading documentation only to figure out that the token you found named `AppSecret` is, somehow, not sensitive at all and meant to be public. This repository is meant to be an inventory of those tokens that look potentially sensitive but aren't so that we can just CTRL-F and save a lot of time.

Contributions are very welcome! See [CONTRIBUTING.md](https://gitlab.com/dee-see/notkeyhacks/-/blob/main/CONTRIBUTING.md).

The wonderful project icon is named [Key On Trash](https://thenounproject.com/term/key-on-trash/1512110/) by [Luis Prado](https://thenounproject.com/Luis/) and licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/us/legalcode).

## Tokens

### Android Apps

- Amazon JWT in `api_key.txt` file
  - https://developer.amazon.com/docs/login-with-amazon/create-android-project.html#add-api-key

- Sift Beacon Key
  - The README shows the key being used in the code so it seems to be normal https://github.com/SiftScience/sift-android

- Sentry DSN (URL with format `https://<key>@sentry.io/<project>`)
  - https://sentry.io/for/android/ shows that it's normal to have the key in the `AndroidManifest.xml` file

- Mixpanel Token
  - https://github.com/mixpanel/mixpanel-android#initialization shows that it's normal to have the token in the code

- Intercom Android
  - The official sample has the key hard-coded https://github.com/intercom/intercom-android/blob/8d6286bf294bd4fb622972c9cafb0da663837dec/samples/fcm-sample/src/main/java/io/intercom/sample/SampleApplication.java#L17

- CodePush `CodePushDeploymentKey`
  - Documentation has the key hard-coded in public https://github.com/microsoft/react-native-code-push/blob/14810b71d79eb769a860c985414cc3b70039508c/docs/setup-android.md

- Singular API key & secret
  - Documentation says to hard-code the key https://github.com/singular-labs/android-sample-app/blob/f4bd0f239efa80ce66d00ff9453e483134725942/README.md

- Stripe key that begins with `pk_`
  - `pk` means publishable key, `sk_` is the sensitive one that shouldn't be leaked https://stripe.com/docs/keys

- Adjust App Token, Push Token, Tracker Token, Purchase Token 
  - All documented in https://github.com/adjust/android_sdk

- `BITMOVIN_API_KEY`
  - Documented in https://github.com/bitmovin/bitmovin-api-sdk-examples

- Salesforce MarketingCloud `access_token` (with marketingcloudapis.com URL)
  - Sample app has it hard-coded https://github.com/salesforce-marketingcloud/MarketingCloudSDK-Android/blob/5df53ffcba87c24a40f39b722f55cd66102cf214/app/src/basic/java/com/salesforce/marketingcloud/learningapp/LearningApplication.kt#L36

- AppDynamics App Key (sometimes called Account Key)
  - It's an identifier more than a "token", not sensitive https://docs.appdynamics.com/display/PRO21/EUM+Accounts%2C+Licenses%2C+and+App+Keys

- `app_secret` in `appcenter-config.json`
  - Documentation hard-codes it https://docs.microsoft.com/en-us/appcenter/sdk/getting-started/android#41-add-the-start-method
  - Sample with the key in the file https://github.com/microsoft/appcenter-sampleapp-react-native/blob/master/android/app/src/main/assets/appcenter-config.json

- **accengage.com** `private` keys
  - In `strings.xml` you may find `acc_private_key` along with `acc_partner_id` but they can't be used for anything.
  - It's the recommended(https://documentation.accengage.com/sdk/android/4.1/gettingStarted/#configure-accengage-credentials) way.

### iPhone Apps

- ...

### Web Apps

- New Relic browser `licenseKey` and `applicationID` found in javascript files
  - Documented in https://docs.newrelic.com/docs/browser/browser-monitoring/configuration/browser-license-key-app-id/
  - Clarification on why the `licenseKey` should be publicly available: https://discuss.newrelic.com/t/security-of-the-browser-agent-license-key/42169
  - Example snippet: `(window.NREUM||(NREUM={})).loader_config={licenseKey:"NRJS-123",applicationID:"123"};`
- `SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED` in React
  - It's just something they put in the framework as a joke https://github.com/facebook/react/pull/4814
